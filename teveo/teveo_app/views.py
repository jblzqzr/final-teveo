from django.shortcuts import render, redirect
from django.db.models import Count
from .utils.list1_parser import parse_listados
from .utils.list2_parser import parse_lista_dos
from .utils.check_timestamp import check_timestamp
from .utils.parse_cctv import parse_cctv
from .utils.download_img import download_image
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
from django.template.loader import render_to_string
from .models import Comments, Cameras, UserProfile, SaveConfig, Like
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.http import HttpResponse, JsonResponse
import base64
from .forms import ChangePreferencesForm, CommentForm
from datetime import datetime
import random
import string
from tzlocal import get_localzone


def get_pages(request, list_page, size_page):
    paginator = Paginator(list_page, size_page)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return page_obj


def transform_list(list_transform):
    list_base64 = []
    for item in list_transform:
        image_binary = item.image
        base64_encoded_image = base64.b64encode(image_binary).decode('utf-8')
        item.image_base64 = base64_encoded_image
        list_base64.append(item)
    return list_base64


def index(request):
    try:
        id_config = request.GET.get('id')
        if id_config is not None:
            try:
                conf = SaveConfig.objects.get(id=id_config)
                request.session['display_name'] = conf.display_name
                request.session['font_family'] = conf.font_family
                request.session['font_size'] = conf.font_size
            except SaveConfig.DoesNotExist:
                print("Wrong ID")
    except KeyError:
        print("No config imported")
    comments = Comments.objects.all().order_by('-date')
    comments_base64 = transform_list(comments)
    page_obj = get_pages(request, comments_base64, 9)
    return render(request, 'teveo_app/index.html', {'page_obj': page_obj})


@csrf_exempt
def camaras(request):
    data_source = ['listado1.xml', 'listado2.xml', 'CCTV.kml']
    if request.method == 'POST':
        qs = request.POST['source']
        if qs == 'listado1.xml':
            parse_listados()
        if qs == 'listado2.xml':
            parse_lista_dos()
        if qs == 'CCTV.kml':
            parse_cctv()
    total_camaras = Cameras.objects.all().count()
    if total_camaras == 0:
        return render(request, 'teveo_app/camaras.html',
                      {'dataSources': data_source})
    else:
        random_imgs = Cameras.objects.order_by('?')
        random_img = random_imgs.first()
        image = check_timestamp(random_img)
        random_img.refresh_from_db()
        if image is None:
            return HttpResponse('Error descarga imagen', status=500)
        image_base64 = base64.b64encode(random_img.image).decode('utf-8')
        img = f'data:image/jpeg;base64,{image_base64}'
        cameras_list = (
            Cameras.objects
            .annotate(num_comments=Count('comments', distinct=True))
            .annotate(num_likes=Count('like', distinct=True))
            .order_by('-num_comments')
        )
        cameras_base64 = transform_list(cameras_list)
        page_obj = get_pages(request, cameras_base64, 9)
    return render(request, 'teveo_app/camaras.html',
                  {'dataSources': data_source,
                   'random_img': random_img,
                   'img': img,
                   'page_obj': page_obj})


def camara(request, id_camara):
    ids_cameras = Cameras.objects.values_list('id', flat=True)
    if id_camara in ids_cameras:
        camera = Cameras.objects.get(id=id_camara)
        check_timestamp(camera)
        camera = Cameras.objects.get(id=id_camara)
        imageb64 = base64.b64encode(camera.image).decode('utf-8')

        comments = Comments.objects.filter(camera=camera).order_by('-date')
        comments_base64 = transform_list(comments)
        page_obj = get_pages(request, comments_base64, 5)

        like_count = Like.objects.filter(camara=camera).count()
        user_id = request.COOKIES.get('sessionid')
        liked = Like.objects.filter(user=user_id, camara=camera).exists()
        return render(request, 'teveo_app/camara.html',
                      {'camara': camera,
                       'imageb64': imageb64,
                       'page_obj': page_obj,
                       'like_count': like_count,
                       'liked': liked})
    else:
        return HttpResponse('id_camara no encontrado')


def dynamic_camara(request, id_camara):
    try:
        camera = Cameras.objects.get(id=id_camara)
        check_timestamp(camera)
        camera = Cameras.objects.get(id=id_camara)
        like_count = Like.objects.filter(camara=camera).count()
        user_id = request.COOKIES.get('sessionid')
        liked = Like.objects.filter(user=user_id, camara=camera).exists()
        return render(request, 'teveo_app/dynamic_camera.html',
                      {'camara': camera,
                       'like_count': like_count,
                       'liked': liked})
    except Cameras.DoesNotExist:
        return HttpResponse('Camara No Existe')


def serve_img(request):
    id_img = request.GET.get('id')[:-1]
    camera = Cameras.objects.get(id=id_img)
    img = download_image(camera.url, camera.id, site="dynamic")
    if img is not None:
        img_bin = base64.b64encode(img).decode('utf-8')

    else:
        img_bin = base64.b64encode(camera.image).decode('utf-8')
    html = f'<img class="img-fluid" src="data:image/jpeg;base64,{img_bin}">'
    return HttpResponse(html, content_type="text/html")


def comments_dyn(request):
    id_com = request.GET.get('id')[:-1]
    camera = Cameras.objects.get(id=id_com)
    comments = Comments.objects.filter(camera=camera).order_by('-date')
    comments_base64 = transform_list(comments)
    page_obj = get_pages(request, comments_base64, 5)
    html = render_to_string('teveo_app/comment_dynamic.html',
                            {'page_obj': page_obj})
    return HttpResponse(html, content_type='text/html')


def show_info_comment(request):
    try:
        id_cam = request.GET.get('id')[:-1]
    except TypeError:
        return HttpResponse('qs no valido')
    try:
        camera = Cameras.objects.get(id=id_cam)
        imageb64 = base64.b64encode(camera.image).decode('utf-8')
        now_utc = datetime.utcnow()
        local_timezone = get_localzone()
        date = now_utc.astimezone(local_timezone)
        form = CommentForm
        return form, date, camera, imageb64
    except Cameras.DoesNotExist:
        return HttpResponse('id no valido')


def comment(request):
    if request.method == 'POST':
        try:
            username = request.session['display_name']
            if username is None:
                username = 'Anónimo'
        except KeyError:
            username = 'Anónimo'

        try:
            id_cam = request.POST.get('id')[:-1]
        except TypeError:
            return HttpResponse('qs no valido')

        camera = Cameras.objects.get(id=id_cam)
        image = camera.image
        now_utc = datetime.utcnow()
        local_timezone = get_localzone()
        date = now_utc.astimezone(local_timezone)
        text = request.POST['content']
        Comments.objects.create(text=text,
                                date=date,
                                image=image,
                                camera=camera,
                                comentator=username)
        if request.POST['redirect'] == 'dyn':
            url_cam = f'/{id_cam}-dyn/'
            return redirect(url_cam)
        url_cam = f'/{id_cam}'
        return redirect(url_cam)

    form, date, camera, imageb64 = show_info_comment(request)
    return render(request, 'teveo_app/comment.html',
                  {'form': form,
                   'time': date,
                   'camera': camera,
                   'image': imageb64})


def modal_content(request):
    form, date, camera, imageb64 = show_info_comment(request)
    return render(request, 'teveo_app/modal_content.html',
                  {'form': form,
                   'time': date,
                   'image': imageb64})


def like(request):
    if request.method == 'POST':
        try:
            camera_id = request.POST.get('id')
            camera = Cameras.objects.get(id=camera_id)
            user_id = request.COOKIES.get('sessionid')
            if not user_id:
                user_id = ''.join(
                    random.choices(string.ascii_lowercase + string.digits,
                                   k=32)
                )
                request.session['user_id'] = user_id

            if Like.objects.filter(user=user_id, camara=camera).exists():
                like_obj = Like.objects.get(user=user_id, camara=camera)
                like_obj.delete()
            else:
                like_obj = Like.objects.create(user=user_id, camara=camera)
                like_obj.save()

            resource = request.POST.get('source')
            url = '/'
            if resource == 'static':
                url += '{}/'.format(camera.id)
            elif resource == 'dynamic':
                url += '{}-dyn/'.format(camera.id)
            return redirect(url)
        except Cameras.DoesNotExist:
            return HttpResponse("Cámara no existe")


def generate_unique_id(request):
    random_number = ''.join(random.choices(string.digits, k=6))
    return (
        f"{request.META.get('REMOTE_ADDR', '')}-"
        f"{request.META.get('HTTP_USER_AGENT', '')}-"
        f"{random_number}"
    )


@csrf_exempt
def config(request):
    user_identifier = generate_unique_id(request)

    if request.user.is_authenticated:
        user = request.user
    else:
        user, _ = User.objects.get_or_create(username='Anónimo')

    try:
        user_profile = UserProfile.objects.get(user=user)
    except UserProfile.DoesNotExist:
        user_profile = None

    if request.method == 'POST':
        if 'logout' in request.POST:
            logout(request)
            response = redirect('/')
            response.delete_cookie('sessionid')
            return response
        form = ChangePreferencesForm(request.POST, instance=user_profile)
        if form.is_valid():
            user_profile = form.save(commit=False)
            user_profile.user = user
            user_profile.identifier = user_identifier
            user_profile.save()
            request.session['display_name'] = form.cleaned_data['display_name']
            request.session['font_family'] = form.cleaned_data['font_family']
            request.session['font_size'] = form.cleaned_data['font_size']
            return redirect('/config/')
    else:
        form = ChangePreferencesForm(instance=user_profile)

    if user_profile and user_profile.display_name:
        user_id = user_profile.display_name
    else:
        user_id = "Anónimo"

    return render(request, 'teveo_app/config.html',
                  {'form': form,
                   'user_id': user_id})


def info(request, id_camara):
    camera = (
        Cameras.objects
        .filter(id=id_camara)
        .annotate(likes=Count('like'))
        .first()
    )
    comments = Comments.objects.filter(camera=camera)

    comments_data = []
    for each_com in comments:
        comment_data = {
            'id': each_com.id,
            'text': each_com.text,
            'date': each_com.date.strftime('%Y-%m-%d %H:%M:%S'),
            'comentator': each_com.comentator,
        }
        comments_data.append(comment_data)

    camera_data = {
        'id': camera.id,
        'name': camera.name,
        'url': camera.url,
        'likes': camera.likes,
        'latitude': camera.latitude,
        'longitude': camera.longitude,
        'lastDownloaded': camera.lastDownloaded.strftime('%Y-%m-%d %H:%M:%S'),
        'comments': comments_data
    }
    return JsonResponse(camera_data)


def help_view(request):
    return render(request, 'teveo_app/help.html')


def generate_id(request):
    valid = False
    while not valid:
        id_new = ''.join(random.choices(
            string.ascii_lowercase + string.digits, k=32))
        if not SaveConfig.objects.filter(id=id_new).exists():
            try:
                display_name = request.session['display_name']
            except KeyError:
                display_name = 'Anónimo'
            try:
                font_family = request.session['font_family']
            except KeyError:
                font_family = 'Montserrat'

            try:
                font_size = request.session['font_size']
            except KeyError:
                font_size = '12px'
            c = SaveConfig.objects.create(id=id_new,
                                          font_family=font_family,
                                          display_name=display_name,
                                          font_size=font_size)
            c.save()
            return HttpResponse(id_new)
