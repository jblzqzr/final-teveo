from xml.dom.minidom import parse
from ..models import Cameras
from .download_img import download_image

def parse_lista_dos():
    with open("../listado2.xml") as file:
        document = parse('../listado2.xml')
        cams = document.getElementsByTagName('cam')
        for cam in cams:
            id = cam.getAttribute('id')
            info = cam.getElementsByTagName('info')[0].firstChild.nodeValue
            url = cam.getElementsByTagName('url')[0].firstChild.nodeValue
            cords = cam.getElementsByTagName('place')
            for cord in cords:
                latitude = cord.getElementsByTagName('latitude')[0].firstChild.nodeValue
                longitude = cord.getElementsByTagName('longitude')[0].firstChild.nodeValue
                if not Cameras.objects.filter(id=id).exists():
                        image = download_image(url, id,'download')
                        c = Cameras.objects.create(id=id,name=info, url=url,latitude=latitude, longitude=longitude, image=image)
                        c.save()



