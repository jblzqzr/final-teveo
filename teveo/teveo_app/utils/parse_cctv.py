from xml.dom.minidom import parse
from .download_img import download_image
from ..models import Cameras


def parse_cctv():
    print("Trying to download CCTV")
    with open("../CCTV.kml") as file:
        document = parse(file)
        placemarks = document.getElementsByTagName('Placemark')

        for placemark in placemarks:
            description = placemark.getElementsByTagName('description')[0].firstChild.nodeValue
            numero = placemark.getElementsByTagName('Data')[0].getElementsByTagName('Value')[0].firstChild.nodeValue
            nombre = placemark.getElementsByTagName('Data')[1].getElementsByTagName('Value')[0].firstChild.nodeValue
            coordinates = placemark.getElementsByTagName('coordinates')[0].firstChild.nodeValue.strip().split(',')

            url_start = description.find('src=') + 4
            url_end = description.find(' width=')
            url = description[url_start:url_end]
            latitude = coordinates[1].strip()
            longitude = coordinates[0].strip()
            if not Cameras.objects.filter(id=numero).exists():
                image = download_image(url, numero, 'download')
                c = Cameras.objects.create(id=numero, name=nombre, url=url, latitude=latitude, longitude=longitude,
                                           image=image)
                c.save()
