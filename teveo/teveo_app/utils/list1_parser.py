from xml.sax import ContentHandler, parse
from ..models import Cameras
from .download_img import download_image
class CameraHandler(ContentHandler):
    def __init__(self):
        self.inCamara = False
        self.inInfo = False
        self.id = ""
        self.src = ""
        self.lugar = ""
        self.coordenadas = ""
        self.cameras = ""
        self.content = ""

    def startElement(self, name, attrs):
        if name == 'camara':
            self.inCamara = True
        elif self.inCamara:
            if name == 'id':
                self.inInfo = True
            elif name == 'src':
                self.inInfo = True
            elif name == 'lugar':
                self.inInfo = True
            elif name == 'coordenadas':
                self.inInfo = True

    def endElement(self, name):
        if name == 'camara':
            self.inCamara = False
            if not Cameras.objects.filter(id=self.id).exists():
                image = download_image(self.src, self.id, 'download')
                c = Cameras.objects.create(id=self.id,name=self.lugar, url=self.src,
                                           latitude=self.latitude, longitude=self.longitude, image=image)
                c.save()
            self.cameras = self.cameras \
                           + f"    <li>ID: {self.id}, Lugar: {self.lugar}, Coordenadas: {self.coordenadas}, <img src='{self.src}'></li>\n"

        elif self.inCamara:
            if name == 'id':
                self.id = self.content
                self.content = ""
                self.inInfo = False
            elif name == 'src':
                self.src = self.content
                self.content = ""
                self.inInfo = False
            elif name == 'lugar':
                self.lugar = self.content
                self.content = ""
                self.inInfo = False
            elif name == 'coordenadas':
                self.coordenadas = self.content
                self.latitude = self.coordenadas.split(",")[0]
                self.longitude = self.coordenadas.split(",")[1]
                self.content = ""
                self.inInfo = False

    def characters(self, chars):
        if self.inInfo:
            self.content = self.content + chars



def parse_listados():
    handler = CameraHandler()
    parse("../listado1.xml", handler)
