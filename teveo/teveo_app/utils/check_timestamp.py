from datetime import datetime
from .download_img import download_image

TIME_BUFFER_IMG = 30

def check_timestamp(camara):
    now = datetime.now().replace(tzinfo=None)
    then = camara.lastDownloaded.replace(tzinfo=None)
    diff = (now - then).total_seconds()
    if diff >= TIME_BUFFER_IMG:
        print('[CONTROL] Downloading new image')
        download_image(camara.url, camara.id)
    return True
