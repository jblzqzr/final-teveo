import urllib.request
from ..models import Cameras


def download_image(url_imagen, camera_id, site="code"):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'}
    """Download image and return it as bytes"""
    request = urllib.request.Request(url=url_imagen, headers=headers)

    try:
        with urllib.request.urlopen(request) as response:
            image = response.read()

            if site == 'code':
                try:
                    camara = Cameras.objects.get(id=camera_id)
                except Cameras.DoesNotExist:
                    return None
                camara.image = image
                camara.save()
                return True
            else:
                return image
    except urllib.error.URLError as e:
        return None
