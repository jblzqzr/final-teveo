from .models import Cameras, Comments


def context_preferences(request):
    font_family = request.session.get('font_family', request.user.userprofile.font_family if hasattr(request.user, 'userprofile') else 'Montserrat')
    font_size = request.session.get('font_size', request.user.userprofile.font_size if hasattr(request.user, 'userprofile') else '16px')

    total_camaras = Cameras.objects.all().count()
    total_comments = Comments.objects.all().count()
    try:
        username = request.session['display_name']
        if username is None:
            username = 'Anónimo'
    except KeyError:
        username = 'Anónimo'

    return {
        'font_family': font_family,
        'font_size': font_size,
        'total_camaras': total_camaras,
        'total_comments': total_comments,
        'username': username,
    }
