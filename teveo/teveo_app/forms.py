from django import forms
from .models import UserProfile


class ChangePreferencesForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['display_name', 'font_family', 'font_size']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['display_name'].initial = None


class CommentForm(forms.Form):
    content = forms.CharField(label='Comment', widget=forms.Textarea)
