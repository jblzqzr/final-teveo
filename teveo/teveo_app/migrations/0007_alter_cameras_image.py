# Generated by Django 5.0.3 on 2024-04-25 13:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teveo_app', '0006_cameras_lastdownloaded'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cameras',
            name='image',
            field=models.CharField(default='', max_length=2048),
        ),
    ]
