from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Cameras(models.Model):
    id = models.CharField(max_length=200, primary_key=True)
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    latitude = models.CharField(max_length=200, default="")
    longitude = models.CharField(max_length=200, default="")
    image = models.BinaryField()
    lastDownloaded = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Comments(models.Model):
    id = models.IntegerField(primary_key=True)
    text = models.CharField(max_length=200)
    date = models.DateTimeField()
    image = models.BinaryField()
    comentator = models.CharField(max_length=50, default='Anónimo')
    camera = models.ForeignKey(Cameras, on_delete=models.CASCADE, related_name='comments')

    def __str__(self):
        return self.text


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=50, blank=True, null=True)
    font_family = models.CharField(max_length=50, blank=True, null=True)
    font_size = models.CharField(max_length=50, blank=True, null=True)
    identifier = models.CharField(max_length=255, unique=True, default="")


class SaveConfig(models.Model):
    id = models.CharField(max_length=32, primary_key=True)
    display_name = models.CharField(max_length=50, blank=True, null=True)
    font_family = models.CharField(max_length=50, blank=True, null=True)
    font_size = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.id


class Like(models.Model):
    user = models.CharField(max_length=50, default='')
    camara = models.ForeignKey(Cameras, on_delete=models.CASCADE, related_name='like')