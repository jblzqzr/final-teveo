from django.contrib import admin
from .models import Cameras, Comments, UserProfile, SaveConfig

# Register your models here.
admin.site.register(Cameras)
admin.site.register(Comments)
admin.site.register(UserProfile)
admin.site.register(SaveConfig)
