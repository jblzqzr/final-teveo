from . import views
from django.urls import path

urlpatterns = [
    path('', views.index, name='index'),
    path('camaras/', views.camaras, name='camaras'),
    path('comentario/', views.comment, name='comment'),
    path('image_embedded/', views.serve_img, name='serve_img'),
    path('comments-dyn/', views.comments_dyn, name='comments_dyn'),
    path('modal-content/', views.modal_content, name='modal_content'),
    path('generate-id/', views.generate_id, name='generate_id'),
    path('<str:id_camara>-dyn/', views.dynamic_camara, name='dynamic_camara'),
    path('like/', views.like, name='like'),
    path('config/', views.config, name='config'),
    path('ayuda/', views.help_view, name='help_view'),
    path('<str:id_camara>/json/', views.info, name='info'),
    path('<str:id_camara>/', views.camara, name='camara'),
]
