from django.test import TestCase, Client
from django.urls import reverse
from .models import Comments, Cameras, SaveConfig, Like, UserProfile
from django.contrib.auth.models import User
from django.contrib.sessions.backends.db import SessionStore
from datetime import datetime
import json

class TestViews(TestCase):

    def test_generate_id_view_without_session_data(self):
        # Realiza una solicitud a la vista generate_id sin datos de sesión
        response = self.client.get(reverse('generate_id'))

        # Verifica que la respuesta sea un código 200, solo que no actualiza nada, lo deja por defecto
        self.assertEqual(response.status_code, 200)

        # Verifica que no se haya creado ninguna configuración en la base de datos
        self.assertTrue(SaveConfig.objects.exists())
    def test_generate_id_view_with_session_data(self):
        # Configura la sesión con datos necesarios


        session = self.client.session
        session['display_name'] = 'Test Display Name'
        session['font_family'] = 'Arial'
        session['font_size'] = '12'
        session.save()

        response = self.client.get(reverse('generate_id'))
        # Verifica que la respuesta sea un código 200
        self.assertEqual(response.status_code, 200)

        # Verifica que se haya creado una nueva configuración en la base de datos
        self.assertTrue(SaveConfig.objects.exists())

        # Configuración creada tiene los datos correctos
        saved_config = SaveConfig.objects.first()
        self.assertEqual(saved_config.display_name, 'Test Display Name')
        self.assertEqual(saved_config.font_family, 'Arial')
        self.assertEqual(saved_config.font_size, '12')

class TestEndToEnd(TestCase):

    def setUp(self):
        self.client = Client()



    def test_camaras_view_without_cameras(self):
        # Realiza una solicitud a la vista camaras cuando no hay cámaras en la base de datos
        response = self.client.get(reverse('camaras'))

        # Verifica que la respuesta sea un código 200
        self.assertEqual(response.status_code, 200)

        # Verifica que se muestre el mensaje de que no hay cámaras disponibles
        self.assertContains(response, "No hay cámaras disponibles")

    def test_index_view_with_comments(self):
        Cameras.objects.create(id='testComment', name="Test Camera", url="https://informo.madrid.es/cameras/Camara06305.jpg?v=21978 ")
        c = Cameras.objects.get(id='testComment')
        # Crea algunos comentarios de prueba en la base de datos
        Comments.objects.create(text="Test comment 1", date=datetime.now(), camera=c)
        Comments.objects.create(text="Test comment 2", date=datetime.now(), camera=c)

        # Realiza una solicitud a la vista index
        response = self.client.get(reverse('index'))

        # Verifica que la respuesta sea un código 200
        self.assertEqual(response.status_code, 200)

        # Verifica que los comentarios se muestran en la página
        self.assertContains(response, "Test comment 1")
        self.assertContains(response, "Test comment 2")
    def test_camara_view_with_valid_id(self):
        # Crea una cámara de prueba en la base de datos
        camera = Cameras.objects.create(id='test', name="Test Camera", url="https://informo.madrid.es/cameras/Camara06305.jpg?v=21978 ")

        # Realiza una solicitud a la vista camara con el ID de la cámara creada
        response = self.client.get(reverse('camara', args=[camera.id]))

        # Verifica que la respuesta sea un código 200
        self.assertEqual(response.status_code, 200)

        # Verifica que se muestre la información de la cámara en la página
        self.assertContains(response, "Test Camera")

        # Verifica imagen embebida
        self.assertContains(response, 'data:image/jpeg;base64')



    def test_comment_view_post_request(self):
        # Crea una cámara de prueba en la base de datos
        camera = Cameras.objects.create(name="Test Camera", url="http://test-url.com")

        # Realiza una solicitud POST a la vista comment con datos de comentario
        response = self.client.post(reverse('comment'),
                                    {'id': f"{camera.id}:", 'content': "Test comment content", 'redirect': 'dyn'})

        # Verifica que la respuesta sea un código 302 (redirección después de la creación exitosa del comentario)
        self.assertEqual(response.status_code, 302)

        # Verifica que se haya creado un comentario en la base de datos
        self.assertTrue(Comments.objects.exists())

        # Verifica que se haya redirigido a la página dinámica de la cámara después de agregar el comentario
        self.assertTrue(response.url.endswith(f'/{camera.id}-dyn/'))

    def test_generate_id_view_with_session_data(self):
        # Configura la sesión con datos necesarios
        session = SessionStore()
        session['display_name'] = 'Test Display Name'
        session['font_family'] = 'Arial'
        session['font_size'] = '12'
        session.save()

        # Simula una solicitud GET a la vista generate_id
        response = self.client.get(reverse('generate_id'), {'id': 'test_id'},
                                   HTTP_COOKIE=f'sessionid={session.session_key}')

        # Verifica que la respuesta sea un código 200
        self.assertEqual(response.status_code, 200)

        # Verifica que se haya creado una nueva configuración en la base de datos
        self.assertTrue(SaveConfig.objects.exists())

        # Verifica que la configuración creada tenga los datos correctos
        saved_config = SaveConfig.objects.first()
        self.assertEqual(saved_config.display_name, 'Test Display Name')
        self.assertEqual(saved_config.font_family, 'Arial')
        self.assertEqual(saved_config.font_size, '12')

    def test_dynamic_camara_view(self):
        self.camera = Cameras.objects.create(id='1', name='Test Camera')
        # Simular una solicitud HTTP GET a la vista dynamic_camara
        response = self.client.get(reverse('dynamic_camara', kwargs={'id_camara': '1'}))

        # Verificar si la solicitud fue exitosa (código de estado 200)
        self.assertEqual(response.status_code, 200)

        # Verificar si los datos de la cámara se pasan al contexto del template
        self.assertEqual(response.context['camara'], self.camera)

        # Verificar si el recuento de likes se pasa al contexto del template
        self.assertEqual(response.context['like_count'], 0)  # Ningún like ha sido dado aún

        # Verificar si el indicador de like se pasa al contexto del template (debe ser falso ya que no se ha dado ningún like)
        self.assertFalse(response.context['liked'])

    def test_like_view(self):
        self.camera = Cameras.objects.create(id='1', name='Test Camera', url='http://example.com/image.jpg')
        # Simular una solicitud HTTP POST a la vista like
        response = self.client.post(reverse('like'), {'id': '1', 'source': 'static'})

        # Verificar si la solicitud fue redirigida correctamente (código de estado 302)
        self.assertEqual(response.status_code, 302)

        # Verificar si se ha creado un nuevo objeto Like en la base de datos
        self.assertTrue(Like.objects.exists())

        # Verificar si se ha dado like a la cámara correctamente
        like_obj = Like.objects.first()
        self.assertEqual(like_obj.user, response.wsgi_request.session['user_id'])
        self.assertEqual(like_obj.camara, self.camera)

    def test_config_view_get(self):
        self.user = User.objects.create(username='testuser')
        self.user_profile = UserProfile.objects.create(user=self.user, display_name='Test User')
        # Iniciar sesión como usuario
        self.client.force_login(self.user)

        # Realizar una solicitud GET a la vista de configuración
        response = self.client.get(reverse('config'))

        # Verificar que la solicitud fue exitosa (código de estado 200)
        self.assertEqual(response.status_code, 200)

        # Verificar que el formulario de preferencias está presente en la respuesta
        self.assertIn('form', response.context)

        # Verificar que el nombre de usuario está presente en la respuesta
        self.assertIn('user_id', response.context)

        # Verificar que el nombre de usuario coincida con el nombre de usuario del perfil del usuario
        self.assertEqual(response.context['user_id'], 'Test User')

    def test_info_view(self):
        #Preparar las camaras
        self.client = Client()
        self.camera = Cameras.objects.create(
            id='testId',
            name='Test Camera',
            url='http://example.com',
            latitude='40.7128',
            longitude='-74.0060',
            lastDownloaded=datetime.now()
        )
        self.comment1 = Comments.objects.create(
            camera=self.camera,
            text='Test Comment 1',
            date=datetime.now(),
            comentator='User1'
        )
        self.comment2 = Comments.objects.create(
            camera=self.camera,
            text='Test Comment 2',
            date=datetime.now(),
            comentator='User2'
        )


        # Realizar una solicitud GET a la vista de info
        response = self.client.get(reverse('info', kwargs={'id_camara': self.camera.id}))

        # Verificar que la solicitud fue exitosa (código de estado 200)
        self.assertEqual(response.status_code, 200)

        # Convertir la respuesta JSON a un diccionario Python
        camera_data = json.loads(response.content)

        # Verificar que los datos de la cámara son correctos
        self.assertEqual(camera_data['id'], self.camera.id)
        self.assertEqual(camera_data['name'], self.camera.name)
        self.assertEqual(camera_data['url'], self.camera.url)

        self.assertEqual(camera_data['latitude'], self.camera.latitude)
        self.assertEqual(camera_data['longitude'], self.camera.longitude)
        self.assertEqual(camera_data['lastDownloaded'], self.camera.lastDownloaded.strftime('%Y-%m-%d %H:%M:%S'))

        # Verificar que los comentarios asociados a la cámara están presentes en la respuesta
        self.assertIn('comments', camera_data)

        # Verificar que los datos de los comentarios son correctos
        self.assertEqual(len(camera_data['comments']), 2)
        self.assertEqual(camera_data['comments'][0]['text'], 'Test Comment 1')
        self.assertEqual(camera_data['comments'][0]['comentator'], 'User1')
        self.assertEqual(camera_data['comments'][1]['text'], 'Test Comment 2')
        self.assertEqual(camera_data['comments'][1]['comentator'], 'User2')
