# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Javier Blázquez Ramírez
* Titulación: Grado en Ingeniería en Tecnologías de la Telecomunicación
* Cuenta en laboratorios: jblzqzr
* Cuenta URJC: j.blazquez.2019@alumnos.urjc.es
* Video básico (url): https://youtu.be/JhCO2TPD6N4
* Video parte opcional (url): https://youtu.be/I7lAUNMqMXo
* Despliegue (url): http://javiblzqz.pythonanywhere.com
* Contraseñas:
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria
Página de ayuda
===============

Web desarrollada por Javier Blázquez Ramírez para la práctica final de la asignatura [**Servicios y Aplicaciones telemáticas**](https://cursosweb.github.io/) del grado de [Ingeniería en Tecnlogías de la Telecomunicación](https://www.urjc.es/universidad/calidad/635-ingenieria-en-tecnologias-de-la-telecomunicacion)  
  
Ahora explicaré como funciona la web.

Nada más entrar en la [web](/), si la base de datos está cargada, y si hubiera comentarios, todos los comentarios, ordenados por fecha de antigüedad, los más recientes primero. Si la base de datos no estuviera cargada, iremos a [Camaras](/camaras/), donde podremos elegir una fuente de datos para descargar, y llenar la base de datos de cámaras. Una vez esté cargada, veremos las cámaras disponibles en esa misma página, como también una cámara aleatoria. Podremos acceder tanto a las páginas dinámicas como estáticas de esas cámaras.

Las páginas estáticas son idénticas, a excepción, de que en las dinámicas, los comentarios y la foto se recargan pasados 30 segundos. En esa misma página, podemos dar like a las camaras, o poner un comentario. También, podemos decirle a la web como nos llamamos, para cuando registremos un comentario, se sepa que hemos sido nosotros. Lo vemos en [Configuración](/config/) Ahí mismo, podemos seleccionar la fuente que queremos usar, entre algunas disponibles, como Angkor, Montserrat, Arial, etc.

Los recursos disponibles son:

*   [TeVeO](/): Página principal
*   [Cámaras](/camaras/): Página de las cámaras
*   [Comentario](/comentario/): Necesita de la qs con la id. Página para poner un comentario
*   [Imágenes](/image_embedded/): Con su qs (id={id}/) en la url, nos da la imagen de la cámara
*   [Comentarios dinámicos](/comments-dyn/): Nos actualiza la informacion de los comentarios
*   [Configuración](/config/): Podemos cambiar algo de la web, como generar un id para copiar la información
*   [Ayuda](/ayuda/): Esta página de ayuda
*   [Página de cada cámara](/<id_camara>/): En cada cámara podremos ver info de la camara, como darle me gusta y dejar un comentario
*   [JSON de cada camara](/<id_camara>/json/): Nos genera un JSON con la información de la cámara
## Lista partes opcionales

* Fuente de datos externa. Cámaras del Ayuntamiento de Madrid:
http://datos.madrid.es/egob/catalogo/202088-0-trafico-camaras.kml
* Inclusión de Favicon.
* Permitir cerrar sesión.
* Posibilidad de dar 'Me Gustas' o 'Likes'
* Algunos tests unitarios
* Paginación en comentarios, cámaras, etc.
